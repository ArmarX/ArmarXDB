If you want to add new queries, their names should be as descriptive as possible. 
Suggestion:
<collection name or shorthand>_<one or more verbs describing the action>_<relevant attribute(s)>_<relevant attribute value(s) or parameter(s) for the action>.js

e.g. a query on Prior_KitchenKKGraphs which only shows entries where the attribute "scene" is "XperienceDemoKitchenRM" could be named:
KitchenKKObjects_filter_scene_XperienceDemoKitchenRM.js
